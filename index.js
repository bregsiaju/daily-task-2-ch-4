// Imports
const express = require('express');
const { PORT = 8000 } = process.env;
const datas = require('./src/data');
const app = express();

// import data handler / filter
const { data1Handler, data2Handler, data3Handler, data4Handler, data5Handler } = require('./src/filtering');

// Static Files
app.use(express.static(__dirname + '/public'));

// set view engine to ejs
app.set('view engine', 'ejs');

// display homepage
app.get('/', (req, res) => {
  res.render('index');
});

// display about page
app.get('/about', (req, res) => {
  res.render('about');
});

// display all data on the table view (ejs)
app.get('/data', (req, res) => {
  res.render('biodata', {
    dataOutput: datas,
    note: 'Tampilkan semua data'
  });
});

// display data filter on the table view (ejs)
app.get('/data1', (req, res) => {
  res.render('biodata', {
    dataOutput: data1Handler(),
    note: 'Age dibawah 30 tahun dan favorit buah pisang'
  });
});

app.get('/data2', (req, res) => {
  res.render('biodata', {
    dataOutput: data2Handler(),
    note: 'Gender female atau company FSW4 dan age diatas 30 tahun'
  });
});

app.get('/data3', (req, res) => {
  res.render('biodata', {
    dataOutput: data3Handler(),
    note: 'Warna mata biru dan age di antara 35 sampai dengan 40, dan favorit buah apel'
  });
});

app.get('/data4', (req, res) => {
  res.render('biodata', {
    dataOutput: data4Handler(),
    note: 'Company Pelangi atau Intel, dan warna mata hijau'
  });
});

app.get('/data5', (req, res) => {
  res.render('biodata', {
    dataOutput: data5Handler(),
    note: 'Registered di bawah tahun 2016 dan masih active (true)'
  });
});

// set tampilan default apa pun url-nya
app.use("/", (req, res) => {
  res.status(404);
  res.render('404');
});

app.listen(PORT, () => {
  console.log(`Server is listening on http://localhost:${PORT}`);
});