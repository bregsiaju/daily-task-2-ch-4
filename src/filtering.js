// import data
const data = require('./data');

const errorHandler = () => {
  const err = {
    message: "data tidak ada / tidak ditemukan"
  };
  return err;
};

const data1Handler = () => {
  const filteredData = [];
  for (let i = 0; i < data.length; i++) {
    // age dibawah 30 tahun dan favorit bua pisang
    if (data[i].age < 30 && data[i].favoriteFruit === "banana") {
      filteredData.push(data[i]);
    }
  }
  return (filteredData.length < 1) ? errorHandler() : filteredData;
};

const data2Handler = () => {
  const filteredData = [];
  for (let i = 0; i < data.length; i++) {
    // gender female atau company FSW4 dan age diatas 30 tahun 
    if ((data[i].gender === "female" || data[i].company === "FSW4") && data[i].age > 30) {
      filteredData.push(data[i]);
    }
  }
  return (filteredData.length < 1) ? errorHandler() : filteredData;
};

const data3Handler = () => {
  const filteredData = [];
  for (let i = 0; i < data.length; i++) {
    // warna mata biru dan age diantara 35 sampai dengan 40, dan favorit buah apel
    if (data[i].eyeColor === "blue" && (data[i].age >= 35 && data[i].age <= 40) && data[i].favoriteFruit === "apple") {
      filteredData.push(data[i]);
    }
  }
  return (filteredData.length < 1) ? errorHandler() : filteredData;
};

const data4Handler = () => {
  const filteredData = [];
  for (let i = 0; i < data.length; i++) {
    // company Pelangi atau Intel, dan warna mata hijau
    if ((data[i].company === "Pelangi" || data[i].company === "Intel") && data[i].eyeColor === "green") {
      filteredData.push(data[i]);
    }
  }

  return (filteredData.length < 1) ? errorHandler() : filteredData;
};

const data5Handler = () => {
  const filteredData = [];
  for (let i = 0; i < data.length; i++) {
    let year = (new Date(data[i].registered)).getFullYear();

    // registered di bawah tahun 2016 dan masih active(true)
    if (year < 2016 && data[i].isActive) {
      filteredData.push(data[i]);
    }
  }
  return (filteredData.length < 1) ? errorHandler() : filteredData;
};

module.exports = { data1Handler, data2Handler, data3Handler, data4Handler, data5Handler };